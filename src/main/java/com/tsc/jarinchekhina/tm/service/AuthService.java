package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IAuthService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyLoginException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyPasswordException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull IUserService userService) {
        this.userService = userService;
    }

    @Nullable
    @Override
    public User getUser() {
        @NotNull final String userId = getUserId();
        @NotNull final Optional<User> user = userService.findById(userId);
        return user.orElse(null);
    }

    @NotNull
    @Override
    @SneakyThrows
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    @SneakyThrows
    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        @NotNull final Role role = user.getRole();
        for (@NotNull final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    @SneakyThrows
    public void login(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.get().getPasswordHash())) throw new AccessDeniedException();
        userId = user.get().getId();
    }

    @Override
    @SneakyThrows
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        userService.create(login, password, email);
    }

}
